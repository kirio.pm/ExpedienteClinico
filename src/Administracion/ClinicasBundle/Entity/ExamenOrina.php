<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamenOrina
 *
 * @ORM\Table(name="examen_orina", indexes={@ORM\Index(name="IDX_FE0F2BFAE38D288B", columns={"consulta_id"}), @ORM\Index(name="IDX_FE0F2BFA677ED5D4", columns={"laboratorio_id"})})
 * @ORM\Entity
 */
class ExamenOrina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="examen_orina_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color_orina", type="string", length=255, nullable=false)
     */
    private $colorOrina;

    /**
     * @var string
     *
     * @ORM\Column(name="bilirubina", type="string", length=255, nullable=false)
     */
    private $bilirubina;

    /**
     * @var string
     *
     * @ORM\Column(name="glucosa", type="string", length=255, nullable=false)
     */
    private $glucosa;

    /**
     * @var string
     *
     * @ORM\Column(name="hemoglobina", type="string", length=255, nullable=false)
     */
    private $hemoglobina;

    /**
     * @var string
     *
     * @ORM\Column(name="cetona", type="string", length=255, nullable=false)
     */
    private $cetona;

    /**
     * @var string
     *
     * @ORM\Column(name="nitritos", type="string", length=255, nullable=false)
     */
    private $nitritos;

    /**
     * @var string
     *
     * @ORM\Column(name="ph", type="string", length=255, nullable=false)
     */
    private $ph;

    /**
     * @var string
     *
     * @ORM\Column(name="densidad", type="string", length=255, nullable=false)
     */
    private $densidad;

    /**
     * @var string
     *
     * @ORM\Column(name="urobilinogeno", type="string", length=255, nullable=false)
     */
    private $urobilinogeno;

    /**
     * @var string
     *
     * @ORM\Column(name="bacterias", type="string", length=255, nullable=false)
     */
    private $bacterias;

    /**
     * @var string
     *
     * @ORM\Column(name="cristales", type="string", length=255, nullable=false)
     */
    private $cristales;

    /**
     * @var string
     *
     * @ORM\Column(name="grasas", type="string", length=255, nullable=false)
     */
    private $grasas;

    /**
     * @var string
     *
     * @ORM\Column(name="mucosidad", type="string", length=255, nullable=false)
     */
    private $mucosidad;

    /**
     * @var string
     *
     * @ORM\Column(name="hematies", type="string", length=255, nullable=false)
     */
    private $hematies;

    /**
     * @var string
     *
     * @ORM\Column(name="celulas_renales", type="string", length=255, nullable=false)
     */
    private $celulasRenales;

    /**
     * @var string
     *
     * @ORM\Column(name="celulas_tubulares", type="string", length=255, nullable=false)
     */
    private $celulasTubulares;

    /**
     * @var string
     *
     * @ORM\Column(name="celulas_epiteliales", type="string", length=255, nullable=false)
     */
    private $celulasEpiteliales;

    /**
     * @var string
     *
     * @ORM\Column(name="leucocitos", type="string", length=255, nullable=false)
     */
    private $leucocitos;

    /**
     * @var \Consulta
     *
     * @ORM\ManyToOne(targetEntity="Consulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consulta_id", referencedColumnName="id")
     * })
     */
    private $consulta;

    /**
     * @var \Laboratorios
     *
     * @ORM\ManyToOne(targetEntity="Laboratorios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="laboratorio_id", referencedColumnName="id")
     * })
     */
    private $laboratorio;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorOrina
     *
     * @param string $colorOrina
     * @return ExamenOrina
     */
    public function setColorOrina($colorOrina)
    {
        $this->colorOrina = $colorOrina;

        return $this;
    }

    /**
     * Get colorOrina
     *
     * @return string 
     */
    public function getColorOrina()
    {
        return $this->colorOrina;
    }

    /**
     * Set bilirubina
     *
     * @param string $bilirubina
     * @return ExamenOrina
     */
    public function setBilirubina($bilirubina)
    {
        $this->bilirubina = $bilirubina;

        return $this;
    }

    /**
     * Get bilirubina
     *
     * @return string 
     */
    public function getBilirubina()
    {
        return $this->bilirubina;
    }

    /**
     * Set glucosa
     *
     * @param string $glucosa
     * @return ExamenOrina
     */
    public function setGlucosa($glucosa)
    {
        $this->glucosa = $glucosa;

        return $this;
    }

    /**
     * Get glucosa
     *
     * @return string 
     */
    public function getGlucosa()
    {
        return $this->glucosa;
    }

    /**
     * Set hemoglobina
     *
     * @param string $hemoglobina
     * @return ExamenOrina
     */
    public function setHemoglobina($hemoglobina)
    {
        $this->hemoglobina = $hemoglobina;

        return $this;
    }

    /**
     * Get hemoglobina
     *
     * @return string 
     */
    public function getHemoglobina()
    {
        return $this->hemoglobina;
    }

    /**
     * Set cetona
     *
     * @param string $cetona
     * @return ExamenOrina
     */
    public function setCetona($cetona)
    {
        $this->cetona = $cetona;

        return $this;
    }

    /**
     * Get cetona
     *
     * @return string 
     */
    public function getCetona()
    {
        return $this->cetona;
    }

    /**
     * Set nitritos
     *
     * @param string $nitritos
     * @return ExamenOrina
     */
    public function setNitritos($nitritos)
    {
        $this->nitritos = $nitritos;

        return $this;
    }

    /**
     * Get nitritos
     *
     * @return string 
     */
    public function getNitritos()
    {
        return $this->nitritos;
    }

    /**
     * Set ph
     *
     * @param string $ph
     * @return ExamenOrina
     */
    public function setPh($ph)
    {
        $this->ph = $ph;

        return $this;
    }

    /**
     * Get ph
     *
     * @return string 
     */
    public function getPh()
    {
        return $this->ph;
    }

    /**
     * Set densidad
     *
     * @param string $densidad
     * @return ExamenOrina
     */
    public function setDensidad($densidad)
    {
        $this->densidad = $densidad;

        return $this;
    }

    /**
     * Get densidad
     *
     * @return string 
     */
    public function getDensidad()
    {
        return $this->densidad;
    }

    /**
     * Set urobilinogeno
     *
     * @param string $urobilinogeno
     * @return ExamenOrina
     */
    public function setUrobilinogeno($urobilinogeno)
    {
        $this->urobilinogeno = $urobilinogeno;

        return $this;
    }

    /**
     * Get urobilinogeno
     *
     * @return string 
     */
    public function getUrobilinogeno()
    {
        return $this->urobilinogeno;
    }

    /**
     * Set bacterias
     *
     * @param string $bacterias
     * @return ExamenOrina
     */
    public function setBacterias($bacterias)
    {
        $this->bacterias = $bacterias;

        return $this;
    }

    /**
     * Get bacterias
     *
     * @return string 
     */
    public function getBacterias()
    {
        return $this->bacterias;
    }

    /**
     * Set cristales
     *
     * @param string $cristales
     * @return ExamenOrina
     */
    public function setCristales($cristales)
    {
        $this->cristales = $cristales;

        return $this;
    }

    /**
     * Get cristales
     *
     * @return string 
     */
    public function getCristales()
    {
        return $this->cristales;
    }

    /**
     * Set grasas
     *
     * @param string $grasas
     * @return ExamenOrina
     */
    public function setGrasas($grasas)
    {
        $this->grasas = $grasas;

        return $this;
    }

    /**
     * Get grasas
     *
     * @return string 
     */
    public function getGrasas()
    {
        return $this->grasas;
    }

    /**
     * Set mucosidad
     *
     * @param string $mucosidad
     * @return ExamenOrina
     */
    public function setMucosidad($mucosidad)
    {
        $this->mucosidad = $mucosidad;

        return $this;
    }

    /**
     * Get mucosidad
     *
     * @return string 
     */
    public function getMucosidad()
    {
        return $this->mucosidad;
    }

    /**
     * Set hematies
     *
     * @param string $hematies
     * @return ExamenOrina
     */
    public function setHematies($hematies)
    {
        $this->hematies = $hematies;

        return $this;
    }

    /**
     * Get hematies
     *
     * @return string 
     */
    public function getHematies()
    {
        return $this->hematies;
    }

    /**
     * Set celulasRenales
     *
     * @param string $celulasRenales
     * @return ExamenOrina
     */
    public function setCelulasRenales($celulasRenales)
    {
        $this->celulasRenales = $celulasRenales;

        return $this;
    }

    /**
     * Get celulasRenales
     *
     * @return string 
     */
    public function getCelulasRenales()
    {
        return $this->celulasRenales;
    }

    /**
     * Set celulasTubulares
     *
     * @param string $celulasTubulares
     * @return ExamenOrina
     */
    public function setCelulasTubulares($celulasTubulares)
    {
        $this->celulasTubulares = $celulasTubulares;

        return $this;
    }

    /**
     * Get celulasTubulares
     *
     * @return string 
     */
    public function getCelulasTubulares()
    {
        return $this->celulasTubulares;
    }

    /**
     * Set celulasEpiteliales
     *
     * @param string $celulasEpiteliales
     * @return ExamenOrina
     */
    public function setCelulasEpiteliales($celulasEpiteliales)
    {
        $this->celulasEpiteliales = $celulasEpiteliales;

        return $this;
    }

    /**
     * Get celulasEpiteliales
     *
     * @return string 
     */
    public function getCelulasEpiteliales()
    {
        return $this->celulasEpiteliales;
    }

    /**
     * Set leucocitos
     *
     * @param string $leucocitos
     * @return ExamenOrina
     */
    public function setLeucocitos($leucocitos)
    {
        $this->leucocitos = $leucocitos;

        return $this;
    }

    /**
     * Get leucocitos
     *
     * @return string 
     */
    public function getLeucocitos()
    {
        return $this->leucocitos;
    }

    /**
     * Set consulta
     *
     * @param \Administracion\ClinicasBundle\Entity\Consulta $consulta
     * @return ExamenOrina
     */
    public function setConsulta(\Administracion\ClinicasBundle\Entity\Consulta $consulta = null)
    {
        $this->consulta = $consulta;

        return $this;
    }

    /**
     * Get consulta
     *
     * @return \Administracion\ClinicasBundle\Entity\Consulta 
     */
    public function getConsulta()
    {
        return $this->consulta;
    }

    /**
     * Set laboratorio
     *
     * @param \Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio
     * @return ExamenOrina
     */
    public function setLaboratorio(\Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio = null)
    {
        $this->laboratorio = $laboratorio;

        return $this;
    }

    /**
     * Get laboratorio
     *
     * @return \Administracion\ClinicasBundle\Entity\Laboratorios 
     */
    public function getLaboratorio()
    {
        return $this->laboratorio;
    }
}
