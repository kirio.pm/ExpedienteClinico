<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Receta
 *
 * @ORM\Table(name="receta", indexes={@ORM\Index(name="IDX_B093494EF625D34B", columns={"id_consulta"}), @ORM\Index(name="IDX_B093494ED18B1B73", columns={"id_medicamento"})})
 * @ORM\Entity
 */
class Receta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="receta_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="firma", type="boolean", nullable=false)
     */
    private $firma;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sello", type="boolean", nullable=false)
     */
    private $sello;

    /**
     * @var \Consulta
     *
     * @ORM\ManyToOne(targetEntity="Consulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_consulta", referencedColumnName="id")
     * })
     */
    private $idConsulta;

    /**
     * @var \Medicamento
     *
     * @ORM\ManyToOne(targetEntity="Medicamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_medicamento", referencedColumnName="id")
     * })
     */
    private $idMedicamento;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Receta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Receta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set firma
     *
     * @param boolean $firma
     * @return Receta
     */
    public function setFirma($firma)
    {
        $this->firma = $firma;

        return $this;
    }

    /**
     * Get firma
     *
     * @return boolean 
     */
    public function getFirma()
    {
        return $this->firma;
    }

    /**
     * Set sello
     *
     * @param boolean $sello
     * @return Receta
     */
    public function setSello($sello)
    {
        $this->sello = $sello;

        return $this;
    }

    /**
     * Get sello
     *
     * @return boolean 
     */
    public function getSello()
    {
        return $this->sello;
    }

    /**
     * Set idConsulta
     *
     * @param \Administracion\ClinicasBundle\Entity\Consulta $idConsulta
     * @return Receta
     */
    public function setIdConsulta(\Administracion\ClinicasBundle\Entity\Consulta $idConsulta = null)
    {
        $this->idConsulta = $idConsulta;

        return $this;
    }

    /**
     * Get idConsulta
     *
     * @return \Administracion\ClinicasBundle\Entity\Consulta 
     */
    public function getIdConsulta()
    {
        return $this->idConsulta;
    }

    /**
     * Set idMedicamento
     *
     * @param \Administracion\ClinicasBundle\Entity\Medicamento $idMedicamento
     * @return Receta
     */
    public function setIdMedicamento(\Administracion\ClinicasBundle\Entity\Medicamento $idMedicamento = null)
    {
        $this->idMedicamento = $idMedicamento;

        return $this;
    }

    /**
     * Get idMedicamento
     *
     * @return \Administracion\ClinicasBundle\Entity\Medicamento 
     */
    public function getIdMedicamento()
    {
        return $this->idMedicamento;
    }
}
