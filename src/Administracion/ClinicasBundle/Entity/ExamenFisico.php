<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ExamenFisico
 *
 * @ORM\Table(name="examen_fisico", indexes={@ORM\Index(name="IDX_6A7BB3CAE38D288B", columns={"consulta_id"}), @ORM\Index(name="IDX_6A7BB3CA677ED5D4", columns={"laboratorio_id"})})
 * @ORM\Entity
 */
class ExamenFisico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="examen_fisico_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_examen", type="string", length=255, nullable=false)
     */
    private $nombreExamen;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     * 
     * @ORM\Column(name="image_path", type="string", length=255)
     * @Assert\File(
     *      maxSize = "2M",
     *      mimeTypes = {"image/jpeg", "image/pjpeg", "image/png", "image/x-png", "image/gif"},
     *      mimeTypesMessage = "este formato de imagen es desconocido",
     *)
     */
    private $imagePath;

    /**
     * @var \Consulta
     *
     * @ORM\ManyToOne(targetEntity="Consulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consulta_id", referencedColumnName="id")
     * })
     */
    private $consulta;

    /**
     * @var \Laboratorios
     *
     * @ORM\ManyToOne(targetEntity="Laboratorios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="laboratorio_id", referencedColumnName="id")
     * })
     */
    private $laboratorio;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreExamen
     *
     * @param string $nombreExamen
     * @return ExamenFisico
     */
    public function setNombreExamen($nombreExamen)
    {
        $this->nombreExamen = $nombreExamen;

        return $this;
    }

    /**
     * Get nombreExamen
     *
     * @return string 
     */
    public function getNombreExamen()
    {
        return $this->nombreExamen;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ExamenFisico
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return ExamenFisico
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return ExamenFisico
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set consulta
     *
     * @param \Administracion\ClinicasBundle\Entity\Consulta $consulta
     * @return ExamenFisico
     */
    public function setConsulta(\Administracion\ClinicasBundle\Entity\Consulta $consulta = null)
    {
        $this->consulta = $consulta;

        return $this;
    }

    /**
     * Get consulta
     *
     * @return \Administracion\ClinicasBundle\Entity\Consulta 
     */
    public function getConsulta()
    {
        return $this->consulta;
    }

    /**
     * Set laboratorio
     *
     * @param \Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio
     * @return ExamenFisico
     */
    public function setLaboratorio(\Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio = null)
    {
        $this->laboratorio = $laboratorio;

        return $this;
    }

    /**
     * Get laboratorio
     *
     * @return \Administracion\ClinicasBundle\Entity\Laboratorios 
     */
    public function getLaboratorio()
    {
        return $this->laboratorio;
    }
}
