<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamenHeces
 *
 * @ORM\Table(name="examen_heces", indexes={@ORM\Index(name="IDX_9C3BB599677ED5D4", columns={"laboratorio_id"}), @ORM\Index(name="IDX_9C3BB599E38D288B", columns={"consulta_id"})})
 * @ORM\Entity
 */
class ExamenHeces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="examen_heces_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="aspecto", type="string", length=50, nullable=false)
     */
    private $aspecto;

    /**
     * @var string
     *
     * @ORM\Column(name="consistencia", type="string", length=50, nullable=false)
     */
    private $consistencia;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=false)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="olor", type="string", length=50, nullable=false)
     */
    private $olor;

    /**
     * @var string
     *
     * @ORM\Column(name="presencia_moco", type="string", length=50, nullable=false)
     */
    private $presenciaMoco;

    /**
     * @var string
     *
     * @ORM\Column(name="presencia_sangre", type="string", length=50, nullable=false)
     */
    private $presenciaSangre;

    /**
     * @var string
     *
     * @ORM\Column(name="restos_alimenticios", type="string", length=50, nullable=false)
     */
    private $restosAlimenticios;

    /**
     * @var string
     *
     * @ORM\Column(name="ph", type="string", length=50, nullable=false)
     */
    private $ph;

    /**
     * @var \Laboratorios
     *
     * @ORM\ManyToOne(targetEntity="Laboratorios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="laboratorio_id", referencedColumnName="id")
     * })
     */
    private $laboratorio;

    /**
     * @var \Consulta
     *
     * @ORM\ManyToOne(targetEntity="Consulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consulta_id", referencedColumnName="id")
     * })
     */
    private $consulta;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aspecto
     *
     * @param string $aspecto
     * @return ExamenHeces
     */
    public function setAspecto($aspecto)
    {
        $this->aspecto = $aspecto;

        return $this;
    }

    /**
     * Get aspecto
     *
     * @return string
     */
    public function getAspecto()
    {
        return $this->aspecto;
    }

    /**
     * Set consistencia
     *
     * @param string $consistencia
     * @return ExamenHeces
     */
    public function setConsistencia($consistencia)
    {
        $this->consistencia = $consistencia;

        return $this;
    }

    /**
     * Get consistencia
     *
     * @return string
     */
    public function getConsistencia()
    {
        return $this->consistencia;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return ExamenHeces
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set olor
     *
     * @param string $olor
     * @return ExamenHeces
     */
    public function setOlor($olor)
    {
        $this->olor = $olor;

        return $this;
    }

    /**
     * Get olor
     *
     * @return string
     */
    public function getOlor()
    {
        return $this->olor;
    }

    /**
     * Set presenciaMoco
     *
     * @param string $presenciaMoco
     * @return ExamenHeces
     */
    public function setPresenciaMoco($presenciaMoco)
    {
        $this->presenciaMoco = $presenciaMoco;

        return $this;
    }

    /**
     * Get presenciaMoco
     *
     * @return string
     */
    public function getPresenciaMoco()
    {
        return $this->presenciaMoco;
    }

    /**
     * Set presenciaSangre
     *
     * @param string $presenciaSangre
     * @return ExamenHeces
     */
    public function setPresenciaSangre($presenciaSangre)
    {
        $this->presenciaSangre = $presenciaSangre;

        return $this;
    }

    /**
     * Get presenciaSangre
     *
     * @return string
     */
    public function getPresenciaSangre()
    {
        return $this->presenciaSangre;
    }

    /**
     * Set restosAlimenticios
     *
     * @param string $restosAlimenticios
     * @return ExamenHeces
     */
    public function setRestosAlimenticios($restosAlimenticios)
    {
        $this->restosAlimenticios = $restosAlimenticios;

        return $this;
    }

    /**
     * Get restosAlimenticios
     *
     * @return string
     */
    public function getRestosAlimenticios()
    {
        return $this->restosAlimenticios;
    }

    /**
     * Set ph
     *
     * @param string $ph
     * @return ExamenHeces
     */
    public function setPh($ph)
    {
        $this->ph = $ph;

        return $this;
    }

    /**
     * Get ph
     *
     * @return string
     */
    public function getPh()
    {
        return $this->ph;
    }

    /**
     * Set laboratorio
     *
     * @param \Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio
     * @return ExamenHeces
     */
    public function setLaboratorio(\Administracion\ClinicasBundle\Entity\Laboratorios $laboratorio = null)
    {
        $this->laboratorio = $laboratorio;

        return $this;
    }

    /**
     * Get laboratorio
     *
     * @return \Administracion\ClinicasBundle\Entity\Laboratorios
     */
    public function getLaboratorio()
    {
        return $this->laboratorio;
    }

    /**
     * Set consulta
     *
     * @param \Administracion\ClinicasBundle\Entity\Consulta $consulta
     * @return ExamenHeces
     */
    public function setConsulta(\Administracion\ClinicasBundle\Entity\Consulta $consulta = null)
    {
        $this->consulta = $consulta;

        return $this;
    }

    /**
     * Get consulta
     *
     * @return \Administracion\ClinicasBundle\Entity\Consulta
     */
    public function getConsulta()
    {
        return $this->consulta;
    }

    public function __toString()
    {
        return $this->id;
    }

}
