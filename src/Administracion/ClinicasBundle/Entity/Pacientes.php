<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pacientes
 *
 * @ORM\Table(name="pacientes")
 * @ORM\Entity
 */
class Pacientes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pacientes_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=100, nullable=false)
     */
    private $apellidos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=false)
     */
    private $fechaNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=150, nullable=false)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="departamento", type="string", length=50, nullable=false)
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="municipio", type="string", length=75, nullable=false)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="dui", type="string", length=10, nullable=true)
     * @Assert\Length(max=10)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="profesion", type="string", length=50, nullable=false)
     */
    private $profesion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefonopadre", type="string", length=9, nullable=true)
     */
    private $telefonopadre;

    /**
     * @var string
     *
     * @ORM\Column(name="telefonomadre", type="string", length=9, nullable=true)
     */
    private $telefonomadre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="esposo", type="boolean", nullable=false)
     */
    private $esposo;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_civil", type="string", length=50, nullable=false)
     */
    private $estadoCivil;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=10, nullable=false)
     */
    private $genero;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad", type="integer", nullable=false)
     */
    private $edad;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Pacientes
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Pacientes
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return Pacientes
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Pacientes
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set departamento
     *
     * @param string $departamento
     * @return Pacientes
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return string
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return Pacientes
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set dui
     *
     * @param string $dui
     * @return Pacientes
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Pacientes
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;

        return $this;
    }

    /**
     * Get profesion
     *
     * @return string
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set telefonopadre
     *
     * @param string $telefonopadre
     * @return Pacientes
     */
    public function setTelefonopadre($telefonopadre)
    {
        $this->telefonopadre = $telefonopadre;

        return $this;
    }

    /**
     * Get telefonopadre
     *
     * @return string
     */
    public function getTelefonopadre()
    {
        return $this->telefonopadre;
    }

    /**
     * Set telefonomadre
     *
     * @param string $telefonomadre
     * @return Pacientes
     */
    public function setTelefonomadre($telefonomadre)
    {
        $this->telefonomadre = $telefonomadre;

        return $this;
    }

    /**
     * Get telefonomadre
     *
     * @return string
     */
    public function getTelefonomadre()
    {
        return $this->telefonomadre;
    }

    /**
     * Set esposo
     *
     * @param boolean $esposo
     * @return Pacientes
     */
    public function setEsposo($esposo)
    {
        $this->esposo = $esposo;

        return $this;
    }

    /**
     * Get esposo
     *
     * @return boolean
     */
    public function getEsposo()
    {
        return $this->esposo;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     * @return Pacientes
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return Pacientes
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Pacientes
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     * @return Pacientes
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer
     */
    public function getEdad()
    {
        return $this->edad;
    }

    public function __toString()
    {
        return $this->nombres . ", " . $this->apellidos;
    }
}
