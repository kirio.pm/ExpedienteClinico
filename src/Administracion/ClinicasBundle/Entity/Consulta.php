<?php

namespace Administracion\ClinicasBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consulta
 *
 * @ORM\Table(name="consulta", indexes={@ORM\Index(name="IDX_A6FE3FDE81DD8AF", columns={"id_signovital"}), @ORM\Index(name="IDX_A6FE3FDE701624C4", columns={"id_expediente"}), @ORM\Index(name="IDX_A6FE3FDE12D224A1", columns={"id_medico"})})
 * @ORM\Entity
 */
class Consulta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="consulta_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_consulta", type="datetime", nullable=false)
     */
    private $fechaConsulta;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_consulta", type="string", length=50, nullable=true)
     */
    private $codigoconsulta;

    /**
     * @var string
     *
     * @ORM\Column(name="diagnostico", type="string", length=100, nullable=true)
     */
     private $diagnostico;

    /**
     * @var \SignoVital
     *
     * @ORM\ManyToOne(targetEntity="SignoVital")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_signovital", referencedColumnName="id")
     * })
     */
    private $idSignovital;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_expediente", referencedColumnName="id")
     * })
     */
    private $idExpediente;

    /**
     * @var \Medicos
     *
     * @ORM\ManyToOne(targetEntity="Medicos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_medico", referencedColumnName="id")
     * })
     */
    private $idMedico;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaConsulta
     *
     * @param \DateTime $fechaConsulta
     * @return Consulta
     */
    public function setFechaConsulta($fechaConsulta)
    {
        $this->fechaConsulta = $fechaConsulta;

        return $this;
    }

    /**
     * Get fechaConsulta
     *
     * @return \DateTime 
     */
    public function getFechaConsulta()
    {
        return $this->fechaConsulta;
    }

    /**
     * Set codigoconsulta
     *
     * @param string $codigoconsulta
     * @return Consulta
     */
    public function setCodigoconsulta($codigoconsulta)
    {
        $this->codigoconsulta = $codigoconsulta;

        return $this;
    }

    /**
     * Get codigoconsulta
     *
     * @return string 
     */
    public function getCodigoconsulta()
    {
        return $this->codigoconsulta;
    }

    /**
     * Set idSignovital
     *
     * @param \Administracion\ClinicasBundle\Entity\SignoVital $idSignovital
     * @return Consulta
     */
    public function setIdSignovital(\Administracion\ClinicasBundle\Entity\SignoVital $idSignovital = null)
    {
        $this->idSignovital = $idSignovital;

        return $this;
    }

    /**
     * Get idSignovital
     *
     * @return \Administracion\ClinicasBundle\Entity\SignoVital 
     */
    public function getIdSignovital()
    {
        return $this->idSignovital;
    }

    /**
     * Set idExpediente
     *
     * @param \Administracion\ClinicasBundle\Entity\Expedientes $idExpediente
     * @return Consulta
     */
    public function setIdExpediente(\Administracion\ClinicasBundle\Entity\Expedientes $idExpediente = null)
    {
        $this->idExpediente = $idExpediente;

        return $this;
    }

    /**
     * Get idExpediente
     *
     * @return \Administracion\ClinicasBundle\Entity\Expedientes 
     */
    public function getIdExpediente()
    {
        return $this->idExpediente;
    }

    /**
     * Set idMedico
     *
     * @param \Administracion\ClinicasBundle\Entity\Medicos $idMedico
     * @return Consulta
     */
    public function setIdMedico(\Administracion\ClinicasBundle\Entity\Medicos $idMedico = null)
    {
        $this->idMedico = $idMedico;

        return $this;
    }

    /**
     * Get idMedico
     *
     * @return \Administracion\ClinicasBundle\Entity\Medicos 
     */
    public function getIdMedico()
    {
        return $this->idMedico;
    }

    public function __toString()
    {
        if(is_null($this->codigoconsulta)) {
            return 'NULL';
        }
        return $this->codigoconsulta;
    }


    /**
     * Set diagnostico
     *
     * @param string $diagnostico
     * @return Consulta
     */
    public function setDiagnostico($diagnostico)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string 
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }
}
