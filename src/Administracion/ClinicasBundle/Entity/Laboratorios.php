<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Laboratorios
 *
 * @ORM\Table(name="laboratorios", indexes={@ORM\Index(name="IDX_742385879CD3F6D6", columns={"clinica_id"})})
 * @ORM\Entity
 */
class Laboratorios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="laboratorios_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var \Clinicas
     *
     * @ORM\ManyToOne(targetEntity="Clinicas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clinica_id", referencedColumnName="id")
     * })
     */
    private $clinica;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Laboratorios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set clinica
     *
     * @param \Administracion\ClinicasBundle\Entity\Clinicas $clinica
     * @return Laboratorios
     */
    public function setClinica(\Administracion\ClinicasBundle\Entity\Clinicas $clinica = null)
    {
        $this->clinica = $clinica;

        return $this;
    }

    /**
     * Get clinica
     *
     * @return \Administracion\ClinicasBundle\Entity\Clinicas
     */
    public function getClinica()
    {
        return $this->clinica;
    }

    public function __toString()
    {
        return $this->nombre;
    }

}
