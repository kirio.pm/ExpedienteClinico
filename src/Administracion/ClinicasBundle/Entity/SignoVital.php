<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignoVital
 *
 * @ORM\Table(name="signo_vital")
 * @ORM\Entity
 */
class SignoVital
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="signo_vital_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="temperatura", type="float", precision=10, scale=0, nullable=false)
     */
    private $temperatura;

    /**
     * @var float
     *
     * @ORM\Column(name="altura", type="float", precision=10, scale=0, nullable=false)
     */
    private $altura;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float", precision=10, scale=0, nullable=false)
     */
    private $peso;

    /**
     * @var float
     *
     * @ORM\Column(name="presion_arterial", type="float", precision=10, scale=0, nullable=false)
     */
    private $presionArterial;

    /**
     * @var float
     *
     * @ORM\Column(name="pulso", type="float", precision=10, scale=0, nullable=false)
     */
    private $pulso;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_ingreso", type="string", length=50, nullable=true)
     */
    private $codigoingreso;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set temperatura
     *
     * @param float $temperatura
     * @return SignoVital
     */
    public function setTemperatura($temperatura)
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    /**
     * Get temperatura
     *
     * @return float 
     */
    public function getTemperatura()
    {
        return $this->temperatura;
    }

    /**
     * Set altura
     *
     * @param float $altura
     * @return SignoVital
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return float 
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set peso
     *
     * @param float $peso
     * @return SignoVital
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float 
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set presionArterial
     *
     * @param float $presionArterial
     * @return SignoVital
     */
    public function setPresionArterial($presionArterial)
    {
        $this->presionArterial = $presionArterial;

        return $this;
    }

    /**
     * Get presionArterial
     *
     * @return float 
     */
    public function getPresionArterial()
    {
        return $this->presionArterial;
    }

    /**
     * Set pulso
     *
     * @param float $pulso
     * @return SignoVital
     */
    public function setPulso($pulso)
    {
        $this->pulso = $pulso;

        return $this;
    }

    /**
     * Get pulso
     *
     * @return float 
     */
    public function getPulso()
    {
        return $this->pulso;
    }

    /**
     * Set codigoingreso
     *
     * @param string $codigoingreso
     * @return SignoVital
     */
    public function setCodigoingreso($codigoingreso)
    {
        $this->codigoingreso = $codigoingreso;

        return $this;
    }

    /**
     * Get codigoingreso
     *
     * @return string 
     */
    public function getCodigoingreso()
    {
        return $this->codigoingreso;
    }

    public function __toString()
    {
        return $this->codigoingreso;
    }
}
