<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SignoVitalRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SignoVitalRepository extends EntityRepository
{
}
