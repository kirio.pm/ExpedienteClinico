<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;


/**
 * Users
 *
 * @ORM\Table(name="users", indexes={@ORM\Index(name="idx_1483a5e94bab96c", columns={"rol_id"}), @ORM\Index(name="IDX_1483A5E99CD3F6D6", columns={"clinica_id"})})
 * @ORM\Entity
 */
class Users implements AdvancedUserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="users_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=100, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * })
     */
    private $rol;

    /**
     * @var \Clinicas
     *
     * @ORM\ManyToOne(targetEntity="Clinicas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clinica_id", referencedColumnName="id")
     * })
     */
    private $clinica;

    protected $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->activo = true;
        $this->rol = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Users
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Users
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set rol
     *
     * @param \Administracion\ClinicasBundle\Entity\Roles $rol
     * @return Users
     */
    public function setRol(\Administracion\ClinicasBundle\Entity\Roles $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \Administracion\ClinicasBundle\Entity\Roles 
     */
     public function getRoles()
     {
       return array(
             'ROLE_USER', new Roles($this) // ROLE dinámico
         );
     
     }

    /**
     * Get rol
     *
     * @return \Administracion\ClinicasBundle\Entity\Roles 
     */
    public function getRol()
    {
        return $this->rol;
    }


    /**
     * Set clinica
     *
     * @param \Administracion\ClinicasBundle\Entity\Clinicas $clinica
     * @return Users
     */
    public function setClinica(\Administracion\ClinicasBundle\Entity\Clinicas $clinica = null)
    {
        $this->clinica = $clinica;

        return $this;
    }

    /**
     * Get clinica
     *
     * @return \Administracion\ClinicasBundle\Entity\Clinicas 
     */
    public function getClinica()
    {
        return $this->clinica;
    }

    public function getSalt()
    {
        return null;
    }


   /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->activo,
            $this->rol,
            $this->clinica
        ));
    }
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->activo,
            $this->rol,
            $this->clinica
        ) = unserialize($serialized);
    }   



     public function isAccountNonExpired()
    {
        return true;
    }
    public function isAccountNonLocked()
    {
        return true;
    }
    public function isCredentialsNonExpired()
    {
        return true;
    }
    public function isEnabled()
    {
        return $this->activo;
    }

     public function eraseCredentials()
    {
        
    }
}
