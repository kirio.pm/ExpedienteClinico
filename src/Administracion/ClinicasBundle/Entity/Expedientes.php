<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientes
 *
 * @ORM\Table(name="expedientes", indexes={@ORM\Index(name="IDX_9F6512717310DAD4", columns={"paciente_id"}), @ORM\Index(name="IDX_9F6512719CD3F6D6", columns={"clinica_id"})})
 * @ORM\Entity
 */
class Expedientes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="expedientes_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=false)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actual", type="date", nullable=false)
     */
    private $fechaActual;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero_exp", type="integer", nullable=false)
     */
    private $numeroExp;

    /**
     * @var \Pacientes
     *
     * @ORM\ManyToOne(targetEntity="Pacientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     * })
     */
    private $paciente;

    /**
     * @var \Clinicas
     *
     * @ORM\ManyToOne(targetEntity="Clinicas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clinica_id", referencedColumnName="id")
     * })
     */
    private $clinica;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Expedientes
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Expedientes
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaActual
     *
     * @param \DateTime $fechaActual
     * @return Expedientes
     */
    public function setFechaActual($fechaActual)
    {
        $this->fechaActual = $fechaActual;

        return $this;
    }

    /**
     * Get fechaActual
     *
     * @return \DateTime 
     */
    public function getFechaActual()
    {
        return $this->fechaActual;
    }

    /**
     * Set numeroExp
     *
     * @param integer $numeroExp
     * @return Expedientes
     */
    public function setNumeroExp($numeroExp)
    {
        $this->numeroExp = $numeroExp;

        return $this;
    }

    /**
     * Get numeroExp
     *
     * @return integer 
     */
    public function getNumeroExp()
    {
        return $this->numeroExp;
    }

    /**
     * Set paciente
     *
     * @param \Administracion\ClinicasBundle\Entity\Pacientes $paciente
     * @return Expedientes
     */
    public function setPaciente(\Administracion\ClinicasBundle\Entity\Pacientes $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \Administracion\ClinicasBundle\Entity\Pacientes 
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set clinica
     *
     * @param \Administracion\ClinicasBundle\Entity\Clinicas $clinica
     * @return Expedientes
     */
    public function setClinica(\Administracion\ClinicasBundle\Entity\Clinicas $clinica = null)
    {
        $this->clinica = $clinica;

        return $this;
    }

    /**
     * Get clinica
     *
     * @return \Administracion\ClinicasBundle\Entity\Clinicas 
     */
    public function getClinica()
    {
        return $this->clinica;
    }
    public function __toString()
    {
        return (string) $this->numeroExp;
    }
}
