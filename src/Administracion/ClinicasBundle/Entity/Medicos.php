<?php

namespace Administracion\ClinicasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medicos
 *
 * @ORM\Table(name="medicos", indexes={@ORM\Index(name="IDX_6450272116A490EC", columns={"especialidad_id"}), @ORM\Index(name="IDX_645027219CD3F6D6", columns={"clinica_id"})})
 * @ORM\Entity
 */
class Medicos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="medicos_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=50, nullable=false)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=50, nullable=false)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="dui", type="string", length=10, nullable=false)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=9, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=100, nullable=false)
     */
    private $correo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="nit", type="string", length=17, nullable=false)
     */
    private $nit;

    /**
     * @var \Especialidades
     *
     * @ORM\ManyToOne(targetEntity="Especialidades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="especialidad_id", referencedColumnName="id")
     * })
     */
    private $especialidad;

    /**
     * @var \Clinicas
     *
     * @ORM\ManyToOne(targetEntity="Clinicas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clinica_id", referencedColumnName="id")
     * })
     */
    private $clinica;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Medicos
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Medicos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set dui
     *
     * @param string $dui
     * @return Medicos
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string 
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Medicos
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Medicos
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Medicos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set nit
     *
     * @param string $nit
     * @return Medicos
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string 
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set especialidad
     *
     * @param \Administracion\ClinicasBundle\Entity\Especialidades $especialidad
     * @return Medicos
     */
    public function setEspecialidad(\Administracion\ClinicasBundle\Entity\Especialidades $especialidad = null)
    {
        $this->especialidad = $especialidad;

        return $this;
    }

    /**
     * Get especialidad
     *
     * @return \Administracion\ClinicasBundle\Entity\Especialidades 
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }

    /**
     * Set clinica
     *
     * @param \Administracion\ClinicasBundle\Entity\Clinicas $clinica
     * @return Medicos
     */
    public function setClinica(\Administracion\ClinicasBundle\Entity\Clinicas $clinica = null)
    {
        $this->clinica = $clinica;

        return $this;
    }

    /**
     * Get clinica
     *
     * @return \Administracion\ClinicasBundle\Entity\Clinicas 
     */
    public function getClinica()
    {
        return $this->clinica;
    }
    public function __toString()
    {
        return $this->nombres;
    }
}
