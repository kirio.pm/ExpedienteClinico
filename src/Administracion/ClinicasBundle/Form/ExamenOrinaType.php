<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExamenOrinaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('consulta')
            ->add('laboratorio')
            ->add('colorOrina')
            ->add('bilirubina')
            ->add('glucosa')
            ->add('hemoglobina')
            ->add('cetona')
            ->add('nitritos')
            ->add('ph')
            ->add('densidad')
            ->add('urobilinogeno')
            ->add('bacterias')
            ->add('cristales')
            ->add('grasas')
            ->add('mucosidad')
            ->add('hematies')
            ->add('celulasRenales')
            ->add('celulasTubulares')
            ->add('celulasEpiteliales')
            ->add('leucocitos')
            ->add('create', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\ExamenOrina'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_examenorina';
    }
}
