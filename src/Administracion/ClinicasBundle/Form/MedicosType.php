<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class MedicosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombres', null, array('attr' => array('maxlength' => 50, 'minlength' => 3)))
            ->add('apellidos', null, array('attr' => array('maxlength' => 50, 'minlength' => 4)))
            ->add('dui', null, array('attr' => array('maxlength' => 10, 'minlength' => 10, 'format' => '00000000-0')))
            ->add('telefono', null, array('attr' => array('maxlength' => 9, 'minlength' => 9)))
            ->add('correo', null, array('attr' => array('maxlength' => 100, 'minlength' => 11, 'format' => 'ejemplo@gmail.com')))
            ->add('especialidad')
            ->add('estado')
            ->add('nit', null, array('attr' => array('maxlength' => 17, 'minlength' => 17, 'format' => '0000-000000-000-0')))
            ->add('clinica')
            //->add('Guardar', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\Medicos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_medicos';
    }
}
