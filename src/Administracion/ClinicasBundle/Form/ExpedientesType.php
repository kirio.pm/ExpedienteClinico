<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExpedientesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estado', 'choice', array('choices' => array(true => 'Vigente',false => 'Caducado')))
            ->add('fechaCreacion', 'date', [
                'widget' => 'single_text'
            ])
            ->add('fechaActual', 'date', [
                'widget' => 'single_text'
            ])
            ->add('paciente')
            ->add('clinica')
            ->add('numeroExp', 'text', array('attr' => array('maxlength' => 6, 'minlength' => 6, 'format' => '0000-00')))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\Expedientes'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_expedientes';
    }
}
