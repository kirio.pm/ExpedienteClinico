<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile; 
use Administracion\ClinicasBundle\Entity\ExamenFisico;

class ExamenFisicoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('consulta')
            ->add('laboratorio')
            ->add('nombreExamen')
            ->add('descripcion')
            ->add('fecha', 'date', [
                'widget' => 'single_text'
            ])
            ->add('imagePath', 'file', array('required' => true, 'data_class' => null, 'label' => 'Seleccione una radiologia'))
            ->add('create', 'submit', array('label' => 'Guardar'))
           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\ExamenFisico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_examenfisico';
    }
    public static function processImage(UploadedFile $uploaded_file, ExamenFisico $entity){
        $path='../../ExpedienteClinico_Symphony/web/images/upload/examenesfisicos/radiologias/';
        $uploaded_file_info = pathinfo($uploaded_file->getClientOriginalName());
        $file_name = $uploaded_file->getClientOriginalName();
        $uploaded_file->move($path,$file_name);
        return $file_name;
    }
}
