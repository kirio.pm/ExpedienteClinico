<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RecetaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'date', [
                'widget' => 'single_text'
            ])
            ->add('descripcion','textarea', array('label' => 'Descripcion'))
            ->add('firma')
            ->add('sello')
            ->add('idConsulta')
            ->add('idMedicamento')
            ->add('receta', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\Receta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_receta';
    }
}
