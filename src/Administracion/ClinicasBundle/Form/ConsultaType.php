<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConsultaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaConsulta', 'date', [
                'widget' => 'single_text'
            ])
            ->add('idSignovital')
            ->add('idExpediente')
            ->add('idMedico')
            ->add('codigoconsulta')
            ->add('diagnostico', 'textarea', array('label' => 'Descripcion del diagnostico'))
            ->add('create', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\Consulta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_consulta';
    }
}
