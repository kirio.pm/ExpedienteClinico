<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\Grouping;

class PacientesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombres', null, array('attr' => array('maxlength' => 50, 'minlength' => 3)))
            ->add('apellidos', null, array('attr' => array('maxlength' => 50, 'minlength' => 4)))
            ->add('fechaNacimiento', 'date', [
                'widget' => 'single_text'
            ])
            ->add('direccion', null, array('attr' => array('maxlength' => 150, 'minlength' => 6)))
            ->add('departamento', null, array('attr' => array('maxlength' => 50, 'minlength' => 6)))
            ->add('municipio', null, array('attr' => array('maxlength' => 75, 'minlength' => 5)))
            ->add('dui', null, array('attr' => array('maxlength' => 10, 'minlength' => 10, 'format' => '00000000-0')))
            ->add('profesion', null, array('attr' => array('maxlength' => 50, 'minlength' => 3)))
            ->add('telefonoPadre', null, array('attr' => array('maxlength' => 9, 'minlength' => 9)))
            ->add('telefonoMadre', null, array('attr' => array('maxlength' => 9, 'minlength' => 9)))
            ->add('esposo', 'choice', array('choices' => array(true => 'Si',false => 'No')))
            ->add('estadoCivil', 'choice', array('choices' => array('Soltero' => 'Soltero','Casado' => 'Casado', 'Viuda' => 'Viuda', 'Divorciado' => 'Dicorciado', 'Acompañado' => 'Acompañado')))
            ->add('genero', 'choice', array('choices' => array('Masculino' => 'Masculino','Femenino' => 'Femenino')))
            ->add('estado')
            ->add('edad', 'text', array('attr' => array('maxlength' => 2, 'minlength' => 1, 'format' => '00')))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\Pacientes'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_pacientes';
    }
}
