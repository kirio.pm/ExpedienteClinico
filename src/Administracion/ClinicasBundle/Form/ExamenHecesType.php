<?php

namespace Administracion\ClinicasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExamenHecesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('aspecto', 'choice', array('choices' => array('Homogéneo' => 'Homogéneo','Heterogéneo' => 'Heterogéneo')))
            ->add('consistencia', 'choice', array('choices' => array('Diarreica' => 'Diarreica','Blanda' => 'Blanda','Pastosa' => 'Pastosa','Formada' => 'Formada')))
            ->add('color', 'choice', array('choices' => array('Café' => 'Café','Blanquecino' => 'Blanquecino','Amarillo' => 'Amarillo','Gris Oscuro' => 'Gris Oscuro','Verde' => 'Verde','Rojo' => 'Rojo','Negro' => 'Negro')))
            ->add('olor', 'choice', array('choices' => array('Sui generis' => 'Sui generis','Necrótico' => 'Necrótico','Acentúado' => 'Acentúado')))
            ->add('presenciaMoco', 'choice', array('choices' => array('Visible' => 'Visible','No Visible' => 'No Visible')))
            ->add('presenciaSangre', 'choice', array('choices' => array('Si' => 'Si','No' => 'No')))
            ->add('restosAlimenticios', 'choice', array('choices' => array('Ausentes' => 'Ausentes','Presentes' => 'Presentes', 'Abundantes' => 'Abundantes')))
            ->add('ph')
            ->add('laboratorio')
            ->add('consulta')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\ClinicasBundle\Entity\ExamenHeces'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_clinicasbundle_examenheces';
    }
}
