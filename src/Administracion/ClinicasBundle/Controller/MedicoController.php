<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Especialidades;
use Administracion\ClinicasBundle\Entity\Medicos;
use Administracion\ClinicasBundle\Form\MedicosType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class MedicoController extends Controller
{
 

  public function indexAction()
  {

      $datos = $this->getDoctrine()->getRepository('ClinicasBundle:Medicos')->findAll();

      return $this->render('ClinicasBundle:Medicos:index.html.twig', compact("datos"));
  }

    public function addAction(Request $request)
  {
      //$form=$this->createForm(new EspecialidadesType());
      //return $this->render('ClinicasBundle:Especialidades:add.html.twig', array("form"=>$form->createView()));
      $especialidad=new Especialidades();
      $medico=new Medicos();
        $form=$this->createForm(new MedicosType(), $medico);

        $form->handleRequest($request);
        if($form->isvalid())
        {
          $em=$this->getDoctrine()->getManager();
          $em->persist($medico);
          $em->flush();

          $this->get('session')->getFlashBag()->add('mensaje', 'Se ha agregado el medico exitosamente');

          return $this->redirect($this->generateUrl('clinicas_medicos_list'));
        }

        return $this->render('ClinicasBundle:Medicos:add.html.twig', array("form"=>$form->createView()));


  }

  public function editAction($id, Request $request)
    {
        $medico=new Medicos();

        $datos=$this->getDoctrine()->getRepository('ClinicasBundle:Medicos')->find($id);
        if(!$datos)
        {
           throw $this->createNotFoundException('No existe el medico con el valor '.$id);
        }

        $form=$this->createForm(new MedicosType(), $datos);
        $form->handleRequest($request);

        if($form->isValid())
        {
           $em=$this->getDoctrine()->getManager();
           $em->flush();
           $this->get('session')->getFlashBag()->add('mensaje', 'Se ha editado el medico exitosamente');

           return $this->redirect($this->generateUrl('clinicas_medicos_list'));
        }

        return $this->render('ClinicasBundle:Medicos:edit.html.twig', array("form"=>$form->createView()));
    }

    public  function deleteAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $medico = $em->getRepository('ClinicasBundle:Medicos')->find($id);

      if (!$medico)
      {
         throw $this->createNotFoundException('No existe el medico con el id '.$id);
      }


      $em->remove($medico);
      $em->flush();

      $this->get('session')->getFlashBag()->add('mensaje', 'Se ha eliminado el medico exitosamente');

      return $this->redirect($this->generateUrl('clinicas_medicos_list'));
    }
}
