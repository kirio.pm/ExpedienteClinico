<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Laboratorios;
use Administracion\ClinicasBundle\Form\LaboratoriosType;

/**
 * Laboratorios controller.
 *
 */
class LaboratoriosController extends Controller
{

    /**
     * Lists all Laboratorios entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Laboratorios')->findAll();

        return $this->render('ClinicasBundle:Laboratorios:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Laboratorios entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Laboratorios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('laboratorios', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Laboratorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Laboratorios entity.
     *
     * @param Laboratorios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Laboratorios $entity)
    {
        $form = $this->createForm(new LaboratoriosType(), $entity, array(
            'action' => $this->generateUrl('laboratorios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Agregar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Laboratorios entity.
     *
     */
    public function newAction()
    {
        $entity = new Laboratorios();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Laboratorios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Laboratorios entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Laboratorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laboratorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Laboratorios:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Laboratorios entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Laboratorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laboratorios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Laboratorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Laboratorios entity.
    *
    * @param Laboratorios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Laboratorios $entity)
    {
        $form = $this->createForm(new LaboratoriosType(), $entity, array(
            'action' => $this->generateUrl('laboratorios_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }
    /**
     * Edits an existing Laboratorios entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Laboratorios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laboratorios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('laboratorios_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Laboratorios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Laboratorios entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Laboratorios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Laboratorios entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('laboratorios'));
    }

    /**
     * Creates a form to delete a Laboratorios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('laboratorios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
