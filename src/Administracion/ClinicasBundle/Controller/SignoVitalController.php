<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\SignoVital;
use Administracion\ClinicasBundle\Form\SignoVitalType;

/**
 * SignoVital controller.
 *
 */
class SignoVitalController extends Controller
{

    /**
     * Lists all SignoVital entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:SignoVital')->findAll();

        return $this->render('ClinicasBundle:SignoVital:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SignoVital entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SignoVital();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('signovital_show', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:SignoVital:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SignoVital entity.
     *
     * @param SignoVital $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SignoVital $entity)
    {
        $form = $this->createForm(new SignoVitalType(), $entity, array(
            'action' => $this->generateUrl('signovital_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SignoVital entity.
     *
     */
    public function newAction()
    {
        $entity = new SignoVital();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:SignoVital:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SignoVital entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:SignoVital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SignoVital entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:SignoVital:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SignoVital entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:SignoVital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SignoVital entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:SignoVital:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SignoVital entity.
    *
    * @param SignoVital $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SignoVital $entity)
    {
        $form = $this->createForm(new SignoVitalType(), $entity, array(
            'action' => $this->generateUrl('signovital_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing SignoVital entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:SignoVital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SignoVital entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('signovital_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:SignoVital:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SignoVital entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:SignoVital')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SignoVital entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('signovital'));
    }

    /**
     * Creates a form to delete a SignoVital entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('signovital_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Elminar'))
            ->getForm()
        ;
    }
}
