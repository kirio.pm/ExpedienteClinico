<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\ExamenFisico;
use Administracion\ClinicasBundle\Form\ExamenFisicoType;

/**
 * ExamenFisico controller.
 *
 */
class ExamenFisicoController extends Controller
{

    /**
     * Lists all ExamenFisico entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:ExamenFisico')->findAll();

        return $this->render('ClinicasBundle:ExamenFisico:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ExamenFisico entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ExamenFisico();
        $form = $this->createCreateForm($entity);
        $request= $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $entity->setImagePath("");
        $form->setData($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {
            if($request= $this->getRequest()->getMethod() == 'POST'){
                $uploaded_file = $form['imagePath']->getData();
                if($uploaded_file){
                    $image = ExamenFisicoType::processImage($uploaded_file,$entity);
                    $entity->setImagePath($image);   
                }
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('examenfisico_show', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:ExamenFisico:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ExamenFisico entity.
     *
     * @param ExamenFisico $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ExamenFisico $entity)
    {
        $form = $this->createForm(new ExamenFisicoType(), $entity, array(
            'action' => $this->generateUrl('examenfisico_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ExamenFisico entity.
     *
     */
    public function newAction()
    {
        $entity = new ExamenFisico();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:ExamenFisico:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ExamenFisico entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenFisico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenFisico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:ExamenFisico:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ExamenFisico entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenFisico')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenFisico entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:ExamenFisico:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ExamenFisico entity.
    *
    * @param ExamenFisico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ExamenFisico $entity)
    {
        $form = $this->createForm(new ExamenFisicoType(), $entity, array(
            'action' => $this->generateUrl('examenfisico_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ExamenFisico entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenFisico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenFisico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $request= $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $entity->setImagePath("");
        $editForm->setData($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if($request= $this->getRequest()->getMethod() == 'PUT'){
                $uploaded_file = $editForm['imagePath']->getData();
                if($uploaded_file){
                    $image = ExamenFisicoType::processImage($uploaded_file,$entity);
                    $entity->setImagePath($image);   
                }
            }
            $em->flush();

            return $this->redirect($this->generateUrl('examenfisico_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:ExamenFisico:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ExamenFisico entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:ExamenFisico')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ExamenFisico entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('examenfisico'));
    }

    /**
     * Creates a form to delete a ExamenFisico entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('examenfisico_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
