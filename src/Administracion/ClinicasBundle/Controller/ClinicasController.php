<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Clinicas;
use Administracion\ClinicasBundle\Form\ClinicasType;

/**
 * Clinicas controller.
 *
 */
class ClinicasController extends Controller
{

    /**
     * Lists all Clinicas entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Clinicas')->findAll();

        return $this->render('ClinicasBundle:Clinicas:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Clinicas entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Clinicas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

             $successMessage = $this->get('translator')->trans('Clinica Creada Exitosamente.');
            $this->addFlash('mensaje', $successMessage);

            return $this->redirect($this->generateUrl('clinicas_show', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Clinicas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Clinicas entity.
     *
     * @param Clinicas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Clinicas $entity)
    {
        $form = $this->createForm(new ClinicasType(), $entity, array(
            'action' => $this->generateUrl('clinicas_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Clinicas entity.
     *
     */
    public function newAction()
    {
        $entity = new Clinicas();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Clinicas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Clinicas entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Clinicas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinicas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Clinicas:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Clinicas entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Clinicas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinicas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Clinicas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Clinicas entity.
    *
    * @param Clinicas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Clinicas $entity)
    {
        $form = $this->createForm(new ClinicasType(), $entity, array(
            'action' => $this->generateUrl('clinicas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Clinicas entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Clinicas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinicas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

              $successMessage = $this->get('translator')->trans('Clinica Actualizada Exitosamente.');
            $this->addFlash('mensaje', $successMessage);

            return $this->redirect($this->generateUrl('clinicas_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Clinicas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Clinicas entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Clinicas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Clinicas entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clinicas'));
    }

    /**
     * Creates a form to delete a Clinicas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clinicas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
