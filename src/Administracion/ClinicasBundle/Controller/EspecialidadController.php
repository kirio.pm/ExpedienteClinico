<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Especialidades;
use Administracion\ClinicasBundle\Form\EspecialidadesType;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EspecialidadController extends Controller
{
  public function indexAction()
  {
      $datos = $this->getDoctrine()->getRepository('ClinicasBundle:Especialidades')->findAll();

      return $this->render('ClinicasBundle:Especialidades:index.html.twig', compact("datos"));
  }

    public function agregarAction(Request $request)
  {

        $especialidad=new Especialidades();
        $form=$this->createForm(new EspecialidadesType(), $especialidad);

        $form->handleRequest($request);
        if($form->isvalid())
        {
          $em=$this->getDoctrine()->getManager();
          $em->persist($especialidad);
          $em->flush();



          $this->get('session')->getFlashBag()->add('mensaje', 'Se ha agregado la especialidad exitosamente');

          return $this->redirect($this->generateUrl('clinicas_especialidades_list'));
        }

        return $this->render('ClinicasBundle:Especialidades:add.html.twig', array("form"=>$form->createView()));


  }

  public function editAction($id, Request $request)
    {
        $especialidad=new Especialidades();

        $datos=$this->getDoctrine()->getRepository('ClinicasBundle:Especialidades')->find($id);
        if(!$datos)
        {
           throw $this->createNotFoundException('No existe la clinica con el valor '.$id);
        }

        $form=$this->createForm(new EspecialidadesType(), $datos);
        $form->handleRequest($request);

        if($form->isValid())
        {
           $em=$this->getDoctrine()->getManager();
           $em->flush();
           $this->get('session')->getFlashBag()->add('mensaje', 'Se ha editado la especialidad exitosamente');

           return $this->redirect($this->generateUrl('clinicas_especialidades_list'));
        }

        return $this->render('ClinicasBundle:Especialidades:edit.html.twig', array("form"=>$form->createView()));
    }

    public  function deleteAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $especialidad = $em->getRepository('ClinicasBundle:Especialidades')->find($id);

      if (!$especialidad)
      {
         throw $this->createNotFoundException('No existe la especialidad con el id '.$id);
      }


      $em->remove($especialidad);
      $em->flush();

      $this->get('session')->getFlashBag()->add('mensaje', 'Se ha eliminado la especialidad exitosamente');

      return $this->redirect($this->generateUrl('clinicas_especialidades_list'));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Especialidades')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especialidades entity.');
        }

        return $this->render('ClinicasBundle:Especialidades:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

    public function especialidadesAction()
{
    $author = new Especialidades();

    // ... do something to the $author object

    $validator = $this->get('validator');
    $errors = $validator->validate($author);

    if (count($errors) > 0) {
        /*
         * Uses a __toString method on the $errors variable which is a
         * ConstraintViolationList object. This gives us a nice string
         * for debugging.
         */
        $errorsString = (string) $errors;

        return new Response($errorsString);
    }

    return new Response('The author is valid! Yes!');
}
}
