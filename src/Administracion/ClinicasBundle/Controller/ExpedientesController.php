<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Expedientes;
use Administracion\ClinicasBundle\Form\ExpedientesType;

/**
 * Expedientes controller.
 *
 */
class ExpedientesController extends Controller
{

    /**
     * Lists all Expedientes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Expedientes')->findAll();

        return $this->render('ClinicasBundle:Expedientes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Expedientes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Expedientes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Se ha cerado el expediente exitosamente');

            return $this->redirect($this->generateUrl('expedientes', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Expedientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Expedientes entity.
     *
     * @param Expedientes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Expedientes $entity)
    {
        $form = $this->createForm(new ExpedientesType(), $entity, array(
            'action' => $this->generateUrl('expedientes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Agregar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Expedientes entity.
     *
     */
    public function newAction()
    {
        $entity = new Expedientes();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Expedientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Expedientes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Expedientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expedientes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Expedientes:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Expedientes entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Expedientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expedientes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Expedientes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Expedientes entity.
    *
    * @param Expedientes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Expedientes $entity)
    {
        $form = $this->createForm(new ExpedientesType(), $entity, array(
            'action' => $this->generateUrl('expedientes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Expedientes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Expedientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expedientes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('expedientes_show', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Expedientes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Expedientes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Expedientes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Expedientes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('expedientes'));
    }

    /**
     * Creates a form to delete a Expedientes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expedientes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
