<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Pacientes;
use Administracion\ClinicasBundle\Form\PacientesType;
use Symfony\Component\HttpFoundation\Request;

class PacienteController extends Controller
{
  public function indexAction()
  {
      $datos = $this->getDoctrine()->getRepository('ClinicasBundle:Pacientes')->findAll();

      return $this->render('ClinicasBundle:Pacientes:index.html.twig', compact("datos"));
  }

    public function agregarAction(Request $request)
  {
      //$form=$this->createForm(new EspecialidadesType());
      //return $this->render('ClinicasBundle:Especialidades:add.html.twig', array("form"=>$form->createView()));

      $paciente=new Pacientes();
        $form=$this->createForm(new PacientesType(), $paciente);

        $form->handleRequest($request);
        if($form->isvalid())
        {
          $em=$this->getDoctrine()->getManager();
          $em->persist($paciente);
          $em->flush();

          $this->get('session')->getFlashBag()->add('mensaje', 'Se ha agregado el paciente exitosamente');

          return $this->redirect($this->generateUrl('clinicas_pacientes_list'));
        }

        return $this->render('ClinicasBundle:Pacientes:add.html.twig', array("form"=>$form->createView()));


  }

  public function editAction($id, Request $request)
    {
        $paciente=new Pacientes();

        $datos=$this->getDoctrine()->getRepository('ClinicasBundle:Pacientes')->find($id);
        if(!$datos)
        {
           throw $this->createNotFoundException('No existe el paciente con el valor '.$id);
        }

        $form=$this->createForm(new PacientesType(), $datos);
        $form->handleRequest($request);

        if($form->isValid())
        {
           $em=$this->getDoctrine()->getManager();
           $em->flush();
           $this->get('session')->getFlashBag()->add('mensaje', 'Se ha editado el paciente exitosamente');

           return $this->redirect($this->generateUrl('clinicas_pacientes_list'));
        }

        return $this->render('ClinicasBundle:Pacientes:edit.html.twig', array("form"=>$form->createView()));
    }

    public  function deleteAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $paciente = $em->getRepository('ClinicasBundle:Pacientes')->find($id);

      if (!$paciente)
      {
         throw $this->createNotFoundException('No existe el paciente con el id '.$id);
      }


      $em->remove($paciente);
      $em->flush();

      $this->get('session')->getFlashBag()->add('mensaje', 'Se ha eliminado el paciente exitosamente');

      return $this->redirect($this->generateUrl('clinicas_pacientes_list'));
    }
}
