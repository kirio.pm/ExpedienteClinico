<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Expedientes;
use Administracion\ClinicasBundle\Form\ExpedientesType;
use Symfony\Component\HttpFoundation\Request;

class ExpedienteController extends Controller
{
  public function indexAction()
  {
      $datos = $this->getDoctrine()->getRepository('ClinicasBundle:Expedientes')->findAll();

      return $this->render('ClinicasBundle:Expedientes:index.html.twig', compact("datos"));
  }

    public function agregarAction(Request $request)
  {
      //$form=$this->createForm(new EspecialidadesType());
      //return $this->render('ClinicasBundle:Especialidades:add.html.twig', array("form"=>$form->createView()));

      $expediente=new Expedientes();
        $form=$this->createForm(new ExpedientesType(), $expediente);

        $form->handleRequest($request);
        if($form->isvalid())
        {
          $em=$this->getDoctrine()->getManager();
          $em->persist($expediente);
          $em->flush();

          $this->get('session')->getFlashBag()->add('mensaje', 'Se ha creado el expediente exitosamente');

          return $this->redirect($this->generateUrl('clinicas_expedientes_list'));
        }

        return $this->render('ClinicasBundle:Expedientes:add.html.twig', array("form"=>$form->createView()));


  }

  public function editAction($id, Request $request)
    {
        $expediente=new Expedientes();

        $datos=$this->getDoctrine()->getRepository('ClinicasBundle:Expedientes')->find($id);
        if(!$datos)
        {
           throw $this->createNotFoundException('No existe el expediente con el valor '.$id);
        }

        $form=$this->createForm(new ExpedientesType(), $datos);
        $form->handleRequest($request);

        if($form->isValid())
        {
           $em=$this->getDoctrine()->getManager();
           $em->flush();
           $this->get('session')->getFlashBag()->add('mensaje', 'Se ha editado el expediente exitosamente');

           return $this->redirect($this->generateUrl('clinicas_expedientes_list'));
        }

        return $this->render('ClinicasBundle:Expedientes:edit.html.twig', array("form"=>$form->createView()));
    }

    public  function deleteAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $expediente = $em->getRepository('ClinicasBundle:Expedientes')->find($id);

      if (!$expediente)
      {
         throw $this->createNotFoundException('No existe el expediente con el id '.$id);
      }


      $em->remove($expediente);
      $em->flush();

      $this->get('session')->getFlashBag()->add('mensaje', 'Se ha eliminado el expediente exitosamente');

      return $this->redirect($this->generateUrl('clinicas_expedientes_list'));
    }
}
