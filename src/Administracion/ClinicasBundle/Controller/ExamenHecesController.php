<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\ExamenHeces;
use Administracion\ClinicasBundle\Form\ExamenHecesType;

/**
 * ExamenHeces controller.
 *
 */
class ExamenHecesController extends Controller
{

    /**
     * Lists all ExamenHeces entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:ExamenHeces')->findAll();

        return $this->render('ClinicasBundle:ExamenHeces:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ExamenHeces entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ExamenHeces();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('examenheces_show', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:ExamenHeces:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ExamenHeces entity.
     *
     * @param ExamenHeces $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ExamenHeces $entity)
    {
        $form = $this->createForm(new ExamenHecesType(), $entity, array(
            'action' => $this->generateUrl('examenheces_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Agregar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new ExamenHeces entity.
     *
     */
    public function newAction()
    {
        $entity = new ExamenHeces();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:ExamenHeces:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ExamenHeces entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenHeces')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenHeces entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:ExamenHeces:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ExamenHeces entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenHeces')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenHeces entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:ExamenHeces:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ExamenHeces entity.
    *
    * @param ExamenHeces $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ExamenHeces $entity)
    {
        $form = $this->createForm(new ExamenHecesType(), $entity, array(
            'action' => $this->generateUrl('examenheces_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }
    /**
     * Edits an existing ExamenHeces entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:ExamenHeces')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExamenHeces entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('examenheces_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:ExamenHeces:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ExamenHeces entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:ExamenHeces')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ExamenHeces entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('examenheces'));
    }

    /**
     * Creates a form to delete a ExamenHeces entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('examenheces_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
