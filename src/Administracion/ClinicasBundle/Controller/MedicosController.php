<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Medicos;
use Administracion\ClinicasBundle\Form\MedicosType;

/**
 * Medicos controller.
 *
 */
class MedicosController extends Controller
{

    /**
     * Lists all Medicos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Medicos')->findAll();

        return $this->render('ClinicasBundle:Medicos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Medicos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Medicos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Se ha agregado el medico exitosamente');

            return $this->redirect($this->generateUrl('medicos', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Medicos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Medicos entity.
     *
     * @param Medicos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Medicos $entity)
    {
        $form = $this->createForm(new MedicosType(), $entity, array(
            'action' => $this->generateUrl('medicos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Agregar'));

        return $form;
    }

    /**
     * Displays a form to create a new Medicos entity.
     *
     */
    public function newAction()
    {
        $entity = new Medicos();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Medicos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Medicos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Medicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medicos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Medicos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Medicos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Medicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medicos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Medicos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Medicos entity.
    *
    * @param Medicos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Medicos $entity)
    {
        $form = $this->createForm(new MedicosType(), $entity, array(
            'action' => $this->generateUrl('medicos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Medicos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Medicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Medicos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('medicos_show', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Medicos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Medicos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Medicos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Medicos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('medicos'));
    }

    /**
     * Creates a form to delete a Medicos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('medicos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Dar de Baja'))
            ->getForm()
        ;
    }
}
