<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Pacientes;
use Administracion\ClinicasBundle\Form\PacientesType;

/**
 * Pacientes controller.
 *
 */
class PacientesController extends Controller
{

    /**
     * Lists all Pacientes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Pacientes')->findAll();

        return $this->render('ClinicasBundle:Pacientes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Pacientes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Pacientes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Se ha agregado el paciente exitosamente');

            return $this->redirect($this->generateUrl('pacientes', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Pacientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Pacientes entity.
     *
     * @param Pacientes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pacientes $entity)
    {
        $form = $this->createForm(new PacientesType(), $entity, array(
            'action' => $this->generateUrl('pacientes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Agregar','attr'=> array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Pacientes entity.
     *
     */
    public function newAction()
    {
        $entity = new Pacientes();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Pacientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Pacientes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Pacientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pacientes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Pacientes:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pacientes entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Pacientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pacientes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Pacientes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Pacientes entity.
    *
    * @param Pacientes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Pacientes $entity)
    {
        $form = $this->createForm(new PacientesType(), $entity, array(
            'action' => $this->generateUrl('pacientes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Pacientes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Pacientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pacientes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pacientes', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Pacientes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Pacientes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Pacientes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pacientes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pacientes'));
    }

    /**
     * Creates a form to delete a Pacientes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pacientes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
