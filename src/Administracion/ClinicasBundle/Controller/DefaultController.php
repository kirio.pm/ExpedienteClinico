<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ClinicasBundle:Default:index.html.twig', array('name' => $name));
    }
}
