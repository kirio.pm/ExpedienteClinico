<?php

namespace Administracion\ClinicasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Administracion\ClinicasBundle\Entity\Receta;
use Administracion\ClinicasBundle\Form\RecetaType;

/**
 * Receta controller.
 *
 */
class RecetaController extends Controller
{

    /**
     * Lists all Receta entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClinicasBundle:Receta')->findAll();

        return $this->render('ClinicasBundle:Receta:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Receta entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Receta();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('receta_show', array('id' => $entity->getId())));
        }

        return $this->render('ClinicasBundle:Receta:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Receta entity.
     *
     * @param Receta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Receta $entity)
    {
        $form = $this->createForm(new RecetaType(), $entity, array(
            'action' => $this->generateUrl('receta_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Receta entity.
     *
     */
    public function newAction()
    {
        $entity = new Receta();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClinicasBundle:Receta:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Receta entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Receta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Receta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Receta:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Receta entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Receta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Receta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClinicasBundle:Receta:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Receta entity.
    *
    * @param Receta $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Receta $entity)
    {
        $form = $this->createForm(new RecetaType(), $entity, array(
            'action' => $this->generateUrl('receta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Receta entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClinicasBundle:Receta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Receta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('receta_edit', array('id' => $id)));
        }

        return $this->render('ClinicasBundle:Receta:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Receta entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClinicasBundle:Receta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Receta entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('receta'));
    }

    /**
     * Creates a form to delete a Receta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('receta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
